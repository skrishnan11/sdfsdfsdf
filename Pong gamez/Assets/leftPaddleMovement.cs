﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class leftPaddleMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.W)) {
			if (transform.position.y <= 1.0)
				transform.position += Vector3.Lerp (Vector3.zero, Vector3.up, 1);


		}

		if (Input.GetKey(KeyCode.S)) {
			if (transform.position.y >= -1.0)
				transform.position += Vector3.Lerp (Vector3.zero, Vector3.down, 1);
			
		}

	}
}
